package com.app;
import java.util.Scanner;


public class UserInput {

    public static void main(String[] args){

        Scanner myObject = new Scanner(System.in); //Creating a scanner object

        System.out.print("Enter a username:");

        String userName = myObject.nextLine();

        System.out.println("Username is:" + userName);

    }
}
