package com.app;
import java.util.Scanner;
public class ActivityAverage {

    public static void main(String[] args) {

            String firstName;
            String lastName;
            Double firstSubject;
            Double secondSubject;
            Double thirdSubject;
            Double average;

            Scanner myObj = new Scanner(System.in);

            System.out.print("First Name:");
            firstName = myObj.nextLine();

            System.out.print("Last Name:");
            lastName = myObj.nextLine();

            System.out.print("First Subject Grade:");
            firstSubject = myObj.nextDouble();

            System.out.print("Second Subject Grade:");
            secondSubject = myObj.nextDouble();

            System.out.print("Third Subject Grade:");
            thirdSubject = myObj.nextDouble();

            average = firstSubject + secondSubject + thirdSubject;
            average = average / 3;

            System.out.print("Good Day," + firstName + " " + lastName + " " + "Your grade is:" + average);
    }
}
